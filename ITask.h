//
// Created by teodorek on 14.08.18.
//

#ifndef THREADPOOL_ITASK_H
#define THREADPOOL_ITASK_H

class ITask
{
public:
    virtual void Execute() = 0;
    virtual ~ITask() = default;
};


#endif //THREADPOOL_ITASK_H
