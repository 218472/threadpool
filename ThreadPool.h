//
// Created by teodorek on 14.08.18.
//

#ifndef THREADPOOL_THREADPOOL_H
#define THREADPOOL_THREADPOOL_H
#include <functional>
#include <vector>
#include <thread>
#include <atomic>
#include <future>
#include <type_traits>
#include "ThreadSafeQueue.h"
#include "ITask.h"
#include "Task.h"

namespace
{
    using sPITask = std::shared_ptr<ITask>;
}; //namespace

class ThreadPool
{
public:
    ThreadPool();
    ~ThreadPool();
    explicit ThreadPool(unsigned int workersNumber);

public:
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool operator = (const ThreadPool &) = delete;

public:
    template<class FunctionT, class... ArgsT>
    auto PushTask(FunctionT &&functor, ArgsT &&... args);
    void TerminateWorkers();

private:
   void WorkerProc();

private:
    std::vector<std::thread>    _workers;
    std::atomic<bool>           _terminate;
    CThreadSafeQueue<sPITask>   _tasks;
};

template<class FunctionT, class... ArgsT>
auto ThreadPool::PushTask(FunctionT &&functor, ArgsT &&... args)
{
    using returnType = typename std::result_of<FunctionT(ArgsT...)>::type;

    auto task = std::make_unique<Task<returnType>>(std::bind(functor, args...));
    auto futureTask = task->GetFuture();

    _tasks.Push(std::move(task));

    return futureTask;
};


#endif //THREADPOOL_THREADPOOL_H
