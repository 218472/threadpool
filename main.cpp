#include <iostream>
#include "ThreadPool.h"

using namespace std::chrono_literals;

template<typename R>
bool is_ready(std::future<R> const& f)
{
    return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

int main()
{
    ThreadPool tp;

    auto task = [] () -> double {
        std::this_thread::sleep_for(2s);
        return 3.14;
    };

    std::vector<std::future<double>> futures;

    futures.emplace_back(tp.Push_task(task));
    futures.emplace_back(tp.Push_task(task));
    futures.emplace_back(tp.Push_task(task));

    //wait for workers
    std::this_thread::sleep_for(2s);

    for(auto & future : futures)
    {
        std::cout<< is_ready(future) ? "Ready" : "Not ready" << std::endl;
    }

    return 0;
}
