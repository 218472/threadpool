//
// Created by teodorek on 14.08.18.
//

#ifndef THREADPOOL_TASK_H
#define THREADPOOL_TASK_H
#include "ITask.h"

template <class T>
class Task : public ITask
{
public:
    Task() = delete;
    Task(std::function<T()> functor) : _task(std::move(functor)){}

public:
    virtual void Execute() override {_task();}
    auto GetFuture() {return _task.get_future();}

private:
    std::packaged_task<T()> _task;
};


#endif //THREADPOOL_TASK_H
