cmake_minimum_required(VERSION 3.10)
project(ThreadPool)

set(CMAKE_CXX_STANDARD 17)
include_directories(../ThreadSafeQueue)
SET(CMAKE_CXX_FLAGS -pthread)

add_executable(ThreadPool main.cpp ITask.h Task.h ThreadPool.cpp ThreadPool.h)
