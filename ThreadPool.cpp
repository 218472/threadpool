//
// Created by teodorek on 14.08.18.
//

#include "ThreadPool.h"

ThreadPool::ThreadPool()
    : _terminate(false)
{
    for(unsigned int i = 0; i < std::thread::hardware_concurrency(); i++)
    {
        _workers.emplace_back(std::thread(&ThreadPool::WorkerProc, this));
    }
}

ThreadPool::ThreadPool(unsigned int workersNumber)
    : _terminate(false)
{
    for(unsigned int i = 0; i < workersNumber; i++)
    {
        _workers.emplace_back(std::thread(&ThreadPool::WorkerProc, this));
    }
}

ThreadPool::~ThreadPool()
{
    TerminateWorkers();
}

void ThreadPool::TerminateWorkers()
{
    _terminate = true;
    _tasks.Invalidate();

    for (auto &worker: _workers)
    {
	worker.join();
    }
}

void ThreadPool::WorkerProc()
{
    while(!_terminate)
    {
        sPITask task;
        if(_tasks.Pop(task))
        {
            task->Execute();
        }
    }
}
